'''
Created on 2 Apr 2010

@author: icamp
'''
import re
import subprocess
import sys



class Connections(object):
    '''
    TCP connection usage information
    '''
    def __init__(self, ports):
        # Set some default ports if none are supplied.
        if ports is None:
            self.ports = [80,443,8553,8080]
        else:
            self.ports = []
            for port in ports.split(','):
                try:
                    self.ports.append(int(port))
                except ValueError:
                    print 'Error: Invalid port "%s".' % port
                    sys.exit(1)
                       
        self.count = {}
        
            
    def _getNetstatOutput(self):        
        try:
            output = subprocess.check_output('netstat -an', shell=True, stderr=subprocess.STDOUT)
            return output.split('\n')            
            
        except subprocess.CalledProcessError, e:
            print e.output
            raise
        
        
    def _countConnection(self, ip, port):
        socket = '%s:%d' % (ip, port)
        if not self.count.has_key(socket):
            self.count[socket] = 0

        self.count[socket] += 1
        
    
    def _formatCount(self):
        count = []        
        for socket in sorted(self.count, key=self.count.__getitem__, reverse=True):
            count.append('%s[%d]' % (socket, self.count[socket]))
            
        return ' '.join(count)
    
    
    def getPortsHeaderString(self):
        return ', '.join(str(port) for port in self.ports)
        
        
    def getConnectionCount(self):
        '''
        Return a count of the current established connections in the following format:
        
        10.2.32.11:80[4] 10.2.32.11:8553[3] 168.0.3.99:443[1] 10.2.32.11:443[1]
        '''
        
        #  TCP    10.0.2.15:59347        213.1.252.141:443      ESTABLISHED
        conn = re.compile('TCP.*\s(\d+\.\d+\.\d+\.\d+):(\d+)\s.*?\s(\d+\.\d+\.\d+\.\d+):(\d+)\s.*ESTABLISHED', re.IGNORECASE)

        for line in self._getNetstatOutput():
            match = conn.search(line)
            if match:               
                _localIP    = match.group(1)
                _localPort  = int(match.group(2))
                _remoteIP   = match.group(3)
                _remotePort = int(match.group(4))
                
                #print('Checking connection: Local %s:%d, Remote %s:%d' % (_localIP, _localPort, _remoteIP, _remotePort))
                
                for port in self.ports:
                    if port == _localPort:
                        self._countConnection(_localIP, _localPort)
                        
                    elif port == _remotePort:
                        self._countConnection(_remoteIP, _remotePort)
        

        return self._formatCount()

