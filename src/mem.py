'''
Created on 2 Apr 2010

@author: icamp
'''
from ctypes import Structure, windll
from ctypes.wintypes import DWORD, c_ulonglong, byref, sizeof



class MEMORYSTATUSEX(Structure):
    _fields_ = [
    ('dwLength', DWORD),
    ('dwMemoryLoad', DWORD),
    ('ullTotalPhys', c_ulonglong),
    ('ullAvailPhys', c_ulonglong),
    ('ullTotalPageFile', c_ulonglong),
    ('ullAvailPageFile', c_ulonglong),
    ('ullTotalVirtual', c_ulonglong),
    ('ullAvailVirtual', c_ulonglong),
    ('ullAvailExtendedVirtual', c_ulonglong),
    ]


class Mem(object):
    '''
    Memory usage information
    '''    
    
    def __init__(self):
        '''
        Constructor
        '''
        self.mem = MEMORYSTATUSEX()
        self.mem.dwLength = sizeof(self.mem)
        kernel32 = windll.LoadLibrary('kernel32.dll')
        kernel32.GlobalMemoryStatusEx(byref(self.mem))
        

    def getTotalMemory(self):
        '''
        Return total physical memory 
        '''
        return self.mem.ullTotalPhys/1024**2
    
    
    def getAvailableMemory(self):
        '''
        Return available physical memory
        '''
        return self.mem.ullAvailPhys/1024**2
    

    def getPercentUsed(self):
        '''
        Return used physical memory as a percentage
        '''
        return self.mem.dwMemoryLoad
        