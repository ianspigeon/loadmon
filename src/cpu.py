'''
Created on 2 Apr 2010

@author: icamp
'''
import time
from ctypes import Structure, Union, windll
from ctypes.wintypes import DWORD, LONG, LPCWSTR, LPCSTR, c_double, byref, c_longlong, HANDLE



class PDH_COUNTER_UNION(Union):
    _fields_ = [('longValue', LONG),
                ('doubleValue', c_double),
                ('largeValue', c_longlong),
                ('AnsiStringValue', LPCSTR),
                ('WideStringValue', LPCWSTR)]

class PDH_FMT_COUNTERVALUE(Structure):
    _fields_ = [('CStatus', DWORD),
                ('union', PDH_COUNTER_UNION),]

Error_Success = 0x00000000
PDH_FMT_LONG = 0x00000100



class Cpu(object):
    '''
    CPU usage information
    '''
    
    def __init__(self):
        '''
        Constructor
        '''
        self.collectionPeriod = 2
        self.hQuery = HANDLE()
        self.hCounter = HANDLE()
        self.pdh = windll.LoadLibrary('pdh.dll') 

        # Setup the CPU performance counter
        if not self.pdh.PdhOpenQueryW(None, 
                                      0, 
                                      byref(self.hQuery)) == Error_Success:
            raise Exception
        
        if not self.pdh.PdhAddCounterW(self.hQuery,
                                       u'''\\Processor(_Total)\\% Processor Time''',
                                       0,
                                       byref(self.hCounter)) == Error_Success:
            raise Exception        
        

    def getCPUUsage(self):
        '''
        Return used CPU percentage
        '''        
        
        # Sample the current CPU value for the sample period
        if not self.pdh.PdhCollectQueryData(self.hQuery) == Error_Success:
            raise Exception
        
        time.sleep(self.collectionPeriod)
        
        if not self.pdh.PdhCollectQueryData(self.hQuery) == Error_Success:
            raise Exception

        # Retrieve sampled counter value
        dwType = DWORD(0)
        value = PDH_FMT_COUNTERVALUE()
        if not self.pdh.PdhGetFormattedCounterValue(self.hCounter,
                                                    PDH_FMT_LONG,
                                                    byref(dwType),
                                                    byref(value)) == Error_Success:
            raise Exception

        # Return percent used
        return value.union.longValue
    

        