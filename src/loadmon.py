'''
Created on 2 Apr 2010

@author: icamp
'''
import os
import sys
import shutil
import platform
import time
from optparse import OptionParser

from mem import Mem
from cpu import Cpu
from connections import Connections 


VERSION = '0.3'



def should_rotate(filename):
    '''
    Determine if the log file should be rotated. Rotates the log each calendar month. 
    '''
    now = time.localtime()
    try:
        with open(filename, 'r') as f:
            lines = f.readlines()
            firstline = lines[1]
            fields = firstline.split(',')
            firstDate = time.strptime(fields[0], '%d/%m/%Y %H:%M:%S')
            
            if now[1] != firstDate[1]:
                return True
    
    except IOError:
        pass
                
    return False    



def rotate_log(filename):
    '''
    Rotate the log file by appending its filename with an incremented number
    '''    
    lastNum = 0
    
    # Loop through each file in the same directory as the log file.
    for root, dirs, files in os.walk(os.path.dirname(filename)):
        for _filename in files:
            
            # Strip the file name from its extension.
            name, ext = os.path.splitext(_filename)
            ext = ext.strip('.')
            
            # Check to see if the file is a rotated log.
            if name == os.path.basename(filename):
                
                try:
                    # Record the number of the last rotated log
                    rotateNum = int(ext)
                    if rotateNum > lastNum:
                        lastNum = rotateNum
                    
                except ValueError:
                    pass
        
        # Avoid recursive search.
        break
         
    # Increment the last rotated log number
    lastNum += 1
    nextNum = '%s.%d' % (filename, lastNum)
    
    # Rotate the log.
    try:
        shutil.move(filename, nextNum)

    except IOError, e:
        print 'Unable to rotate file "%s", Error: %s' %(filename, e)
        sys.exit(1)
    


if __name__ == '__main__':
    
    usage = 'usage: %prog [options]'
    parser = OptionParser(usage, version='Version: %s' % VERSION)

    parser.add_option('-f', '--file', dest='filename', 
                      help='Output the result to a file instead of the screen')
    
    parser.add_option('-d', '--debug', action="store_true", 
                      help='Enable debug information')
    
    parser.add_option('-p', '--ports', dest='ports', 
                      help='A comma separated list of ports used to count the number of establish connections per port')        


    (options, args) = parser.parse_args()
    

    if platform.system() != 'Windows':
        print 'Error: Currently only windows is supported'
        sys.exit(1)
       
    
    cpu = Cpu()
    mem = Mem()
    connections = Connections(options.ports)
    
    if options.filename:
        
        filename = options.filename
        
        # Format the load results in CSV format      
        data = '%s,%s,%d,%d,%s\n' %(time.strftime('%d/%m/%Y %H:%M:%S'),
                                        platform.node(),
                                        cpu.getCPUUsage(), 
                                        mem.getPercentUsed(),
                                        connections.getConnectionCount())

        # Rotate log each month
        if should_rotate(filename):
            rotate_log(filename)
                
            
        # If the file does not already exist prefix the results with a header line.
        if not os.path.exists(filename):
            header = '# Date, Hostname, CPU%%, Mem%%, Connections(%s)\n' % connections.getPortsHeaderString()
            data = header + data


        # Append the results to the specified log file
        try:                
            with open(filename, 'a') as f:
                f.write(data)    
        
        except IOError, e:
            print 'Unable to save to file "%s", Error: %s' %(filename, e)
            sys.exit(1)  
    
    
    else:
        
        # Display the load results to the screen.        
        print '              Usage'   
        print 'CPU:          %4d%%' % cpu.getCPUUsage()
        print 'Mem:          %4d%%' % mem.getPercentUsed() 

        # Display each port connection count on a separate line. 
        conns = connections.getConnectionCount().split(' ')
        firstConn = conns[0] if (len(conns) >= 1 and conns[0] != '') else 'No monitored connections found.'
        print 'Connections:  %s' % firstConn        
        if len(conns) >= 2: 
            for conn in conns[1:]: 
                print '              %s'    % conn
                
            
        print ''
        print 'Note: Monitored connections are limited to ports; %s.'    % connections.getPortsHeaderString()
        
        if options.debug: 
            print '\n** DEBUG INFORMATION **'
            print 'Hostname: %s' % platform.node()
            print 'Mem TotalPhys: %d' % mem.getTotalMemory()
            print 'Mem AvailPhys: %d' % mem.getAvailableMemory()
    

    